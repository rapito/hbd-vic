Meteor.startup( ->
  Session.set 'started', false
)

Template.slider.rendered = ->
  startSlider()

Template.slider.helpers
  images: ->
    images = Session.get 'images'
    images ?= Meteor.call 'getImages', (err, res)->
      Session.set 'images', res
#      console.log 'res', res
#      console.log 'err', err
      Meteor.setTimeout(startSlider,1000)

    images

  randomAlign: ->
    _.sample ['center', 'left', 'right']

  insult: (image)->
    v = "insult-#{image}"
    console.log v
    insult = Session.get v
    insult ?= Meteor.call 'getInsult', (err, res) ->
      Session.set v, res
#      console.log 'res', res
#      console.log 'err', err

    insult

Template.startButton.events =
  'click #start-button': ()->
    Session.set 'started', true
    startSlider()

Template.content.helpers
  started: ->
    Session.get 'started'

startSlider = ()->
  $('.slider').slider()